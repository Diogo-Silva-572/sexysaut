# GIT Cheat Sheet

https://education.github.com/git-cheat-sheet-education.pdf

# NOTE: Antes de correrem a simulação têm de alterar um parâmetro para ativar os lasers do Lidar
1. Vão a Files - other Locations
2. Entrem no Computador
3. Pastas: opt -> ros -> melodic (ou o que caralho decidiram instalar) -> share -> husky_description -> urdf
4. No ficheiro husky.urdf.xacro alterar na linha com "laser_enabled", dentro de parentesis, o bit 0 para 1

# Para darem build do projeto quando fazem alterações
1. source ~/sexysaut/code/devel/setup.bash (para cada terminal que abrirem)
2. apaguem as pastas build e devel
3. catkin_make (dentro da pasta code)

# TO-DO
- Validar centroides calculados
- Calcular variancia
- Micro simulador: melhorar visualização, mostrar média pesada das particulas, experimentar com variancias baixas
- Exportar os algoritmos do matlab para o ros

# Simulação
Variancia sensor: x=0.002 y=0.0011
Raio=0.132

```
$ roscore
$ roslaunch myhusky_gazebo myhusky.launch
$ roslaunch perception perception.launch
$ roslaunch fastslam fastslam.launch
$ rostopic pub -r 1 /cmd_vel geometry_msgs/Twist '[0.2, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
```

# Organização

```
├── code
    ├── build
    ├── devel
    ├── src
        ├── myhusky_gazebo       #node que lança o husky e o mapa
            ├── launch           #contém o launch file
            ├── worlds           #mundos
        ├── perception
            ├── launch
            ├── rviz             #tem o ficheiro para o rviz lançar com a configuraçao pretendida
            ├── msg              #cone.msg && cone_detections.msg
            ├── src 
                ├── perception_main.cpp #não tem processamento nenhum
                ├── perception_handle.cpp #tem o codigo que serve para receber e publicar topicos
                ├── perception.cpp #tem o algoritmo de processamento da perceção(a magia é aqui)
            ├── include          #são headers
``` 
