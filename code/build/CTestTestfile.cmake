# CMake generated Testfile for 
# Source directory: /home/diogo/sexysaut/code/src
# Build directory: /home/diogo/sexysaut/code/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("launchall")
subdirs("fastslam")
subdirs("myhusky_gazebo")
subdirs("perception")
