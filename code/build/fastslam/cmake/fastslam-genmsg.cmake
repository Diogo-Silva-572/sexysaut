# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "fastslam: 2 messages, 0 services")

set(MSG_I_FLAGS "-Ifastslam:/home/diogo/sexysaut/code/src/fastslam/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/melodic/share/sensor_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/melodic/share/geometry_msgs/cmake/../msg;-Inav_msgs:/opt/ros/melodic/share/nav_msgs/cmake/../msg;-Itf:/opt/ros/melodic/share/tf/cmake/../msg;-Ipcl_msgs:/opt/ros/melodic/share/pcl_msgs/cmake/../msg;-Iactionlib_msgs:/opt/ros/melodic/share/actionlib_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(fastslam_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg" NAME_WE)
add_custom_target(_fastslam_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fastslam" "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg" "pcl_msgs/PointIndices:geometry_msgs/Point:std_msgs/Header"
)

get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg" NAME_WE)
add_custom_target(_fastslam_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fastslam" "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg" "pcl_msgs/PointIndices:geometry_msgs/Point:fastslam/cone:std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fastslam
)
_generate_msg_cpp(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fastslam
)

### Generating Services

### Generating Module File
_generate_module_cpp(fastslam
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fastslam
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(fastslam_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(fastslam_generate_messages fastslam_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_cpp _fastslam_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_cpp _fastslam_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fastslam_gencpp)
add_dependencies(fastslam_gencpp fastslam_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fastslam_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fastslam
)
_generate_msg_eus(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fastslam
)

### Generating Services

### Generating Module File
_generate_module_eus(fastslam
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fastslam
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(fastslam_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(fastslam_generate_messages fastslam_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_eus _fastslam_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_eus _fastslam_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fastslam_geneus)
add_dependencies(fastslam_geneus fastslam_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fastslam_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fastslam
)
_generate_msg_lisp(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fastslam
)

### Generating Services

### Generating Module File
_generate_module_lisp(fastslam
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fastslam
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(fastslam_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(fastslam_generate_messages fastslam_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_lisp _fastslam_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_lisp _fastslam_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fastslam_genlisp)
add_dependencies(fastslam_genlisp fastslam_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fastslam_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fastslam
)
_generate_msg_nodejs(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fastslam
)

### Generating Services

### Generating Module File
_generate_module_nodejs(fastslam
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fastslam
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(fastslam_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(fastslam_generate_messages fastslam_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_nodejs _fastslam_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_nodejs _fastslam_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fastslam_gennodejs)
add_dependencies(fastslam_gennodejs fastslam_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fastslam_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fastslam
)
_generate_msg_py(fastslam
  "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/melodic/share/pcl_msgs/cmake/../msg/PointIndices.msg;/opt/ros/melodic/share/geometry_msgs/cmake/../msg/Point.msg;/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg;/opt/ros/melodic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fastslam
)

### Generating Services

### Generating Module File
_generate_module_py(fastslam
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fastslam
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(fastslam_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(fastslam_generate_messages fastslam_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/cone.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_py _fastslam_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/diogo/sexysaut/code/src/fastslam/msg/ConeDetections.msg" NAME_WE)
add_dependencies(fastslam_generate_messages_py _fastslam_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fastslam_genpy)
add_dependencies(fastslam_genpy fastslam_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fastslam_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fastslam)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fastslam
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(fastslam_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(fastslam_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(fastslam_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()
if(TARGET nav_msgs_generate_messages_cpp)
  add_dependencies(fastslam_generate_messages_cpp nav_msgs_generate_messages_cpp)
endif()
if(TARGET tf_generate_messages_cpp)
  add_dependencies(fastslam_generate_messages_cpp tf_generate_messages_cpp)
endif()
if(TARGET pcl_msgs_generate_messages_cpp)
  add_dependencies(fastslam_generate_messages_cpp pcl_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fastslam)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fastslam
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(fastslam_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(fastslam_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(fastslam_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()
if(TARGET nav_msgs_generate_messages_eus)
  add_dependencies(fastslam_generate_messages_eus nav_msgs_generate_messages_eus)
endif()
if(TARGET tf_generate_messages_eus)
  add_dependencies(fastslam_generate_messages_eus tf_generate_messages_eus)
endif()
if(TARGET pcl_msgs_generate_messages_eus)
  add_dependencies(fastslam_generate_messages_eus pcl_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fastslam)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fastslam
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(fastslam_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(fastslam_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(fastslam_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()
if(TARGET nav_msgs_generate_messages_lisp)
  add_dependencies(fastslam_generate_messages_lisp nav_msgs_generate_messages_lisp)
endif()
if(TARGET tf_generate_messages_lisp)
  add_dependencies(fastslam_generate_messages_lisp tf_generate_messages_lisp)
endif()
if(TARGET pcl_msgs_generate_messages_lisp)
  add_dependencies(fastslam_generate_messages_lisp pcl_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fastslam)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fastslam
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(fastslam_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(fastslam_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(fastslam_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()
if(TARGET nav_msgs_generate_messages_nodejs)
  add_dependencies(fastslam_generate_messages_nodejs nav_msgs_generate_messages_nodejs)
endif()
if(TARGET tf_generate_messages_nodejs)
  add_dependencies(fastslam_generate_messages_nodejs tf_generate_messages_nodejs)
endif()
if(TARGET pcl_msgs_generate_messages_nodejs)
  add_dependencies(fastslam_generate_messages_nodejs pcl_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fastslam)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fastslam\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fastslam
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(fastslam_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(fastslam_generate_messages_py sensor_msgs_generate_messages_py)
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(fastslam_generate_messages_py geometry_msgs_generate_messages_py)
endif()
if(TARGET nav_msgs_generate_messages_py)
  add_dependencies(fastslam_generate_messages_py nav_msgs_generate_messages_py)
endif()
if(TARGET tf_generate_messages_py)
  add_dependencies(fastslam_generate_messages_py tf_generate_messages_py)
endif()
if(TARGET pcl_msgs_generate_messages_py)
  add_dependencies(fastslam_generate_messages_py pcl_msgs_generate_messages_py)
endif()
