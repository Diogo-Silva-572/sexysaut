
(cl:in-package :asdf)

(defsystem "fastslam-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :pcl_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "ConeDetections" :depends-on ("_package_ConeDetections"))
    (:file "_package_ConeDetections" :depends-on ("_package"))
    (:file "cone" :depends-on ("_package_cone"))
    (:file "_package_cone" :depends-on ("_package"))
  ))