(cl:in-package fastslam-msg)
(cl:export '(HEADER-VAL
          HEADER
          POSITION-VAL
          POSITION
          ID-VAL
          ID
          POINT_INDICES-VAL
          POINT_INDICES
))