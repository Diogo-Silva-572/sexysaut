; Auto-generated. Do not edit!


(cl:in-package fastslam-msg)


;//! \htmlinclude cone.msg.html

(cl:defclass <cone> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (position
    :reader position
    :initarg :position
    :type geometry_msgs-msg:Point
    :initform (cl:make-instance 'geometry_msgs-msg:Point))
   (id
    :reader id
    :initarg :id
    :type cl:integer
    :initform 0)
   (point_indices
    :reader point_indices
    :initarg :point_indices
    :type pcl_msgs-msg:PointIndices
    :initform (cl:make-instance 'pcl_msgs-msg:PointIndices)))
)

(cl:defclass cone (<cone>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <cone>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'cone)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name fastslam-msg:<cone> is deprecated: use fastslam-msg:cone instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <cone>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader fastslam-msg:header-val is deprecated.  Use fastslam-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <cone>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader fastslam-msg:position-val is deprecated.  Use fastslam-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'id-val :lambda-list '(m))
(cl:defmethod id-val ((m <cone>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader fastslam-msg:id-val is deprecated.  Use fastslam-msg:id instead.")
  (id m))

(cl:ensure-generic-function 'point_indices-val :lambda-list '(m))
(cl:defmethod point_indices-val ((m <cone>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader fastslam-msg:point_indices-val is deprecated.  Use fastslam-msg:point_indices instead.")
  (point_indices m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <cone>) ostream)
  "Serializes a message object of type '<cone>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'position) ostream)
  (cl:let* ((signed (cl:slot-value msg 'id)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'point_indices) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <cone>) istream)
  "Deserializes a message object of type '<cone>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'position) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'id) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'point_indices) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<cone>)))
  "Returns string type for a message object of type '<cone>"
  "fastslam/cone")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'cone)))
  "Returns string type for a message object of type 'cone"
  "fastslam/cone")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<cone>)))
  "Returns md5sum for a message object of type '<cone>"
  "fd8d64bfc1ad4dd282fedca83514c41c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'cone)))
  "Returns md5sum for a message object of type 'cone"
  "fd8d64bfc1ad4dd282fedca83514c41c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<cone>)))
  "Returns full string definition for message of type '<cone>"
  (cl:format cl:nil "std_msgs/Header header          # cone header~%geometry_msgs/Point position    # coordinate of cone in [x, y, z]~%int32 id                        # cone ID from tracking~%pcl_msgs/PointIndices point_indices # pointcloud points of the cone~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: pcl_msgs/PointIndices~%Header header~%int32[] indices~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'cone)))
  "Returns full string definition for message of type 'cone"
  (cl:format cl:nil "std_msgs/Header header          # cone header~%geometry_msgs/Point position    # coordinate of cone in [x, y, z]~%int32 id                        # cone ID from tracking~%pcl_msgs/PointIndices point_indices # pointcloud points of the cone~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: pcl_msgs/PointIndices~%Header header~%int32[] indices~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <cone>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'position))
     4
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'point_indices))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <cone>))
  "Converts a ROS message object to a list"
  (cl:list 'cone
    (cl:cons ':header (header msg))
    (cl:cons ':position (position msg))
    (cl:cons ':id (id msg))
    (cl:cons ':point_indices (point_indices msg))
))
