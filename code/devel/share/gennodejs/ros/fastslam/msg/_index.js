
"use strict";

let cone = require('./cone.js');
let ConeDetections = require('./ConeDetections.js');

module.exports = {
  cone: cone,
  ConeDetections: ConeDetections,
};
