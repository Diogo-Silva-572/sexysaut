;; Auto-generated. Do not edit!


(when (boundp 'perception::cone)
  (if (not (find-package "PERCEPTION"))
    (make-package "PERCEPTION"))
  (shadow 'cone (find-package "PERCEPTION")))
(unless (find-package "PERCEPTION::CONE")
  (make-package "PERCEPTION::CONE"))

(in-package "ROS")
;;//! \htmlinclude cone.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "PCL_MSGS"))
  (ros::roseus-add-msgs "pcl_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass perception::cone
  :super ros::object
  :slots (_header _position _id _point_indices ))

(defmethod perception::cone
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:position __position) (instance geometry_msgs::Point :init))
    ((:id __id) 0)
    ((:point_indices __point_indices) (instance pcl_msgs::PointIndices :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _position __position)
   (setq _id (round __id))
   (setq _point_indices __point_indices)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:position
   (&rest __position)
   (if (keywordp (car __position))
       (send* _position __position)
     (progn
       (if __position (setq _position (car __position)))
       _position)))
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:point_indices
   (&rest __point_indices)
   (if (keywordp (car __point_indices))
       (send* _point_indices __point_indices)
     (progn
       (if __point_indices (setq _point_indices (car __point_indices)))
       _point_indices)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Point _position
    (send _position :serialization-length)
    ;; int32 _id
    4
    ;; pcl_msgs/PointIndices _point_indices
    (send _point_indices :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Point _position
       (send _position :serialize s)
     ;; int32 _id
       (write-long _id s)
     ;; pcl_msgs/PointIndices _point_indices
       (send _point_indices :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Point _position
     (send _position :deserialize buf ptr-) (incf ptr- (send _position :serialization-length))
   ;; int32 _id
     (setq _id (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; pcl_msgs/PointIndices _point_indices
     (send _point_indices :deserialize buf ptr-) (incf ptr- (send _point_indices :serialization-length))
   ;;
   self)
  )

(setf (get perception::cone :md5sum-) "fd8d64bfc1ad4dd282fedca83514c41c")
(setf (get perception::cone :datatype-) "perception/cone")
(setf (get perception::cone :definition-)
      "std_msgs/Header header          # cone header
geometry_msgs/Point position    # coordinate of cone in [x, y, z]
int32 id                        # cone ID from tracking
pcl_msgs/PointIndices point_indices # pointcloud points of the cone

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: pcl_msgs/PointIndices
Header header
int32[] indices


")



(provide :perception/cone "fd8d64bfc1ad4dd282fedca83514c41c")


