#include "ros/ros.h"
#include "fastslam/fastslam_handle.hpp"


int main(int argc, char** argv) {

	ros::init(argc, argv, "fastslam");

	ros::NodeHandle nh;
	FastslamHandle FastslamHandle(nh);

	ros::Rate loop_rate(10);

	while (ros::ok()) {

		FastslamHandle.run();	// run node

			// Keeps node alive basically
		ros::spinOnce();
	}

	return 0;
}
