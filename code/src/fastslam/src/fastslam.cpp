#include "fastslam/fastslam.hpp"

std::default_random_engine rng;

Fastslam::Fastslam() {

}

//getter
visualization_msgs::MarkerArray Fastslam::getParticlesViz(){
	return _markerArray;
}

void Fastslam::setCones(const perception::ConeDetections &cones){
    _Cones.clear();
    for(int i=0; i<cones.cone_detections.size(); i++){
        Point q(cones.cone_detections[i].position.x, cones.cone_detections[i].position.y);
        _Cones.push_back(q);
    }
}
void Fastslam::setOdom(const geometry_msgs::Twist &odom){
    ROS_WARN("RECEBEU--------\n");
    _received=true;
    if(odom.linear.x>10 || odom.linear.x<-10 || odom.linear.y>10 || odom.linear.y<-10 || odom.angular.z>10 || odom.angular.z<-10){
        ROS_WARN("--------APANHOU LIXO\n");
        _odomParams.vx=0;
        _odomParams.vy=0;
        _odomParams.theta=0;
    }else{

        _odomParams.vx=odom.linear.x;
        _odomParams.vy=odom.linear.y;
        _odomParams.theta=odom.angular.z;
        ROS_WARN("Callback THETA: %f\n", _odomParams.theta); 
        ROS_WARN("Callback P-X: %f\n", _odomParams.vx);
        ROS_WARN("Callback P-Y: %f\n", _odomParams.vy);  
    }
    

}

void Fastslam::runAlgorithm(){
    float _NumParticles = 30;

    //Define initial Position
    Pose initial_position;
    initial_position.x = 0;
    initial_position.y = 0;
    initial_position.theta = 0;
    
    if(!_initialization){
        initParticles(initial_position, _NumParticles);
        _initialization = true;
    }
    if(_received){
        prediction_Step(_NumParticles);
        createMarkers(_NumParticles);

        _received=false;
    }
    
    //_particles.clear();
}   


      
void Fastslam::initParticles(Pose initial_position, float _NumParticles) {
    
    Particle p;
    for (int i = 0; i < _NumParticles; i++){
        
        p.weight = 1/_NumParticles;
        p.pose.x = 0; 
        p.pose.y = 0; 
        p.pose.theta = 0; 
  
       if(!_landmarks.empty()) {
            for(Landmark &landmark: _landmarks) {
                p.particle_landmarks.push_back(landmark);
            }
        }

        _particles.push_back(p);
    }
}

void Fastslam::prediction_Step(float _NumParticles){
    
    for(int i = 0; i < _NumParticles; i++){
       _particles[i] = Motion_Model(_particles[i]);
    }
}

float Fastslam::normalize_angle(float theta){
      if(theta > M_PI){
        theta = theta - 2 * M_PI;
      }
      if(theta < -1*M_PI){
        theta = theta + 2 * M_PI;
      }
      return theta;
}

Particle Fastslam::Motion_Model(Particle particle){
      float std_dev_dx = 0.1;
      float std_dev_dy = 0.1; 
      float std_dev_theta = 0.005;
       
       std::normal_distribution<float> posx(0, std_dev_dx);
       std::normal_distribution<float> posy(0, std_dev_dy);
       std::normal_distribution<float> angle(0, std_dev_theta);

       float random_x = posx(rng);
       float random_y = posy(rng); 
       float random_theta = angle(rng);

       float dv = hypot(_odomParams.vx + random_x, _odomParams.vy + random_y)* 0.1; 

      particle.pose.x = particle.pose.x + cos(random_theta + _odomParams.theta*0.1 + particle.pose.theta)*dv;
      particle.pose.y = particle.pose.y + sin(random_theta + _odomParams.theta*0.1 + particle.pose.theta)*dv; 
      particle.pose.theta = random_theta + particle.pose.theta +_odomParams.theta*0.1;

    //   ROS_WARN("Particle THETA: %f\n", _particles[i].pose.theta); 
    //   ROS_WARN("Particle P-X: %f\n", _particles[i].pose.x);
    //   ROS_WARN("Particle P-Y: %f\n", _particles[i].pose.y); 

     return particle;
}


visualization_msgs::Marker Fastslam::mark_def(visualization_msgs::Marker marker, int i){
    
        marker.id = i;

        marker.header.frame_id = "/base_laser";
        marker.ns = "my_namespace";
        marker.action = visualization_msgs::Marker::ADD;
        marker.header.stamp = ros::Time::now();
        marker.type = visualization_msgs::Marker::SPHERE;
        
        marker.scale.x = 0.1;
        marker.scale.y = 0.1;
        marker.scale.z = 0.1;
        
        marker.color.r=1;
        marker.color.g=0.5;
        marker.color.b=0.5;
        marker.color.a=1;

        marker.pose.position.z = 0;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;

        return marker;
}

void Fastslam::createMarkers(int _NumParticles){
    //See Particle Markers in RViz
    std::vector<visualization_msgs::Marker> markerArray(_NumParticles);

    for(int i = 0; i < _NumParticles; i++){
       visualization_msgs::Marker marker_buff;
       
       marker_buff.pose.position.x = _particles[i].pose.x;
       marker_buff.pose.position.y = _particles[i].pose.y;

       marker_buff = mark_def(marker_buff, i);

       markerArray[i] = marker_buff;
    }
    _markerArray.markers = markerArray;
}

void cumulativeSum(const std::vector<float>& input, std::vector<float>& result) {
	result.push_back(input[0]);
	for (int i = 1; i < input.size(); i++) {
		result.push_back(result[i - 1] + input[i]);
	}
}

/*
    -Numero particulas _Numparticles
    -vetor de particulas
*/
std::vector<Particle> Fastslam::Resample(std::vector<Particle> particles, float _NumParticles){
    
    std::vector<Particle> new_particles;
    float sum_weights = 0;
    float weightSum = 0;
    std::vector<float> cum_weight;
    std::vector<float> weight_vector;
    
    for(int i=0; i<_NumParticles; i++){
        weight_vector.push_back(particles[i].weight);
    }

    for (auto& n : weight_vector){
        sum_weights += n;
    }
    
    for(int i=0; i<_NumParticles; i++){
        weight_vector[i] = weight_vector[i]/sum_weights;
    }

    cumulativeSum(weight_vector, cum_weight);

    weightSum = cum_weight[_NumParticles];

    float step = weightSum/_NumParticles;
    int idx = 0;

    std::default_random_engine generator;
    std::normal_distribution<float> distribution(0.0,weightSum);
    
    float position = distribution(generator);
    
    for(int i = 0; i<_NumParticles; i++){
        position = position + step;
        if(position > weightSum){
            position = position - weightSum;
            idx=1;
        }
        while(position > cum_weight[idx]){
            idx++;
        }
        Particle buf;
        buf = particles[idx];
        buf.weight = 1/_NumParticles;
        new_particles.push_back(buf);
    }

    return new_particles;
}

