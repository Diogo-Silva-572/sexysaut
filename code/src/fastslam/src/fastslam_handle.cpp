#include "fastslam/fastslam_handle.hpp"		// include header file


FastslamHandle::FastslamHandle(ros::NodeHandle &nodeHandle): _nodeHandle(nodeHandle) {
    _pubParticlesViz=_nodeHandle.advertise<visualization_msgs::MarkerArray>("particles_rviz",1);
    subscribeToTopics();
}

void FastslamHandle::subscribeToTopics(){
    _subCones = _nodeHandle.subscribe("/cone_coords",1, &FastslamHandle::coneDetectionsCallback,this);
    _subOdom = _nodeHandle.subscribe("/cmd_vel",1, &FastslamHandle::odomCallback,this);
}

void FastslamHandle::run() {
	_subpub.runAlgorithm();
    _pubParticlesViz.publish(_subpub.getParticlesViz());
}

void FastslamHandle::coneDetectionsCallback(const perception::ConeDetections &conesDetected){
    _subpub.setCones(conesDetected);
}

void FastslamHandle::odomCallback(const geometry_msgs::Twist &odom){
    _subpub.setOdom(odom);
}