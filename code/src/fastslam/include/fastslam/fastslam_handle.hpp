#ifndef FASTSLAM_HANDLE_HPP
#define FASTSLAM_HANDLE_HPP

#include "ros/ros.h"
#include "fastslam/fastslam.hpp"	// include the Example class (header file)
#include "fastslam/fastslam_structures.hpp"

class FastslamHandle {

	public:

		// constructor
		FastslamHandle(ros::NodeHandle &nodeHandle);

		// methods
		void run();
		void subscribeToTopics();

	private:

		// attributes
		ros::NodeHandle _nodeHandle;
		ros::Publisher _pubOutput;
		ros::Publisher _pubParticlesViz;
		ros::Subscriber _subCones;
		ros::Subscriber _subOdom;
		Fastslam _subpub;		// instance of the algorithm itself

		
		void coneDetectionsCallback(const perception::ConeDetections &conesDetected);
		void odomCallback(const geometry_msgs::Twist &odom);
};

#endif
