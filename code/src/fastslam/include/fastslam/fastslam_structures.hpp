#ifndef FASTSLAM_TYPES_HPP
#define FASTSLAM_TYPES_HPP

#include <eigen3/Eigen/Eigen>
#include <utility>
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include <nav_msgs/Path.h>


typedef struct OdomParam{
	float vx;
	float vy;
	float theta;
}OdomParam;

typedef struct Pose{
	float x;
	float y;
	float theta;
};

typedef struct Landmark {
    Eigen::Vector2f mu;    // landmark position mean
    Eigen::Matrix2f sigma; // landmark position covariance

};

typedef struct Particle{
    
    float weight;
	Pose pose;

    std::vector<Landmark> particle_landmarks;

};


#endif