#ifndef FASTSLAM_HPP
#define FASTSLAM_HPP

#include "ros/ros.h"
#include <stdio.h>
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Twist.h"
#include "message_filters/subscriber.h"
#include <visualization_msgs/MarkerArray.h>
#include "perception/cone.h"
#include "perception/ConeDetections.h"
#include <cmath>
#include <math.h>
#include <iostream>
#include <random>
#include <chrono>
#include <vector>
#include <Eigen/Core>

#include <ros/node_handle.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/PoseArray.h>
#include "std_msgs/Int16.h"
#include "std_msgs/Bool.h"

#include "fastslam_structures.hpp"

typedef Eigen::Vector2d Point;

class Fastslam {
	
	public:

		// constructor
		Fastslam();

		// getters
		visualization_msgs::MarkerArray getParticlesViz();

		// setters
		void setCones(const perception::ConeDetections &);
		void setOdom(const geometry_msgs::Twist &);
		
		// methods
		void runAlgorithm();
		void initParticles(Pose initial_position, float _NumParticles);
		void prediction_Step(float _NumParticles);
		float normalize_angle(float theta);
		void createMarkers(int _NumParticles);
		
		Particle Motion_Model(Particle Particle);

		visualization_msgs::Marker mark_def(visualization_msgs::Marker marker, int i);
		std::vector<Particle> Resample(std::vector<Particle> , float );
		
		

	private:

		// attributes
		std::vector<Point> _Cones;
        std::vector<Landmark> _landmarks;

		OdomParam _odomParams;

		std::vector<Particle> _particles;
		
		visualization_msgs::MarkerArray _markerArray;

		float _Nparticles;
		bool _initialization = false;
		bool _received = false;
};

#endif
