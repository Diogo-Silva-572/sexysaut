#include "perception/perception.hpp"

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

std::vector<pcl::PointIndices> _clusterIndices;
perception::ConeDetections _clusterDetections;
PclPointCloud::Ptr pcl_cloud_buf(new PclPointCloud);
PclPointCloud::Ptr pcl_cloud(new PclPointCloud);
sensor_msgs::PointCloud2 cloud;
std::vector<std::vector<float>> allCentroids;

// constructor 
Perception::Perception() {}

// getter
perception::ConeDetections Perception::getPointCoords() {
	return _output;
}

visualization_msgs::MarkerArray Perception::getConesRviz(){
	return _markerArray;
}

visualization_msgs::MarkerArray Perception::getPointCloudRviz(){
    return _markerArrayPointCloud;
}

// setter
void Perception::setLaser(const sensor_msgs::LaserScan& inputData) {
	_input = inputData;		// save value
}

std::vector<pcl::PointIndices> const & getClusterIndices(PclPointCloud::Ptr pointCloud) {
	_clusterIndices.clear();

	pcl::io::savePCDFileASCII ("test_pcd.pcd", (*pcl_cloud));

	pcl::search::KdTree<PclPoint>::Ptr tree(new pcl::search::KdTree<PclPoint>);
	tree->setInputCloud(pointCloud);

	pcl::EuclideanClusterExtraction<PclPoint> ec;
	ec.setClusterTolerance(0.80);
	ec.setMinClusterSize(1);
	ec.setMaxClusterSize(150);
	ec.setSearchMethod(tree);
	ec.setInputCloud(pointCloud);
	ec.extract(_clusterIndices);

	return _clusterIndices;
}

std::vector<float> ransacCloud(PclPointCloud::Ptr pointCloud) {

	pcl::ModelCoefficients::Ptr coef(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    pcl::SACSegmentation<PclPoint> seg;

    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_CIRCLE3D);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.05);
    seg.setMaxIterations(100);
    seg.setInputCloud(pointCloud);
    seg.segment(*inliers, *coef);
	
    return coef->values;
}

void computeCentroids(std::vector<pcl::PointIndices> idx, PclPointCloud::Ptr pcl_cloud){

    std::vector<float> centroids;
    for(int i=0; i<idx.size(); i++){
        
        if(idx[i].indices.size()>=3){
            
            pcl::PointCloud<PclPoint>::Ptr cloud1(new pcl::PointCloud<PclPoint>);
            
            cloud1->is_dense=false;
            cloud1->points.resize(idx[i].indices.size());

            for(int j=0; j<idx[i].indices.size(); j++){
                cloud1->points[j].x=pcl_cloud->points[idx[i].indices[j]].x;
                cloud1->points[j].y=pcl_cloud->points[idx[i].indices[j]].y;
                cloud1->points[j].z=pcl_cloud->points[idx[i].indices[j]].z;
            }
            centroids=ransacCloud(cloud1);            
            allCentroids.push_back(centroids);

        }
    }

}

void centroid2msg(std::vector<pcl::PointIndices> idx){

    std::vector<perception::cone> centroidArray(idx.size());

    for(size_t i=0; i<allCentroids.size(); i++){
        centroidArray[i].position.x=allCentroids[i][0];
        centroidArray[i].position.y=allCentroids[i][1];
        centroidArray[i].position.z=0;
        centroidArray[i].header=cloud.header;
    }
    _clusterDetections.cone_detections=centroidArray;
    _clusterDetections.header=cloud.header;
}

void computeVariance() {

    std::vector<Point> varArray;
    int i = 0;
	Point var = Point::Zero();
	//ROS_WARN("Numero de clouds %d\n",  _clusterIndices.size());

	for(int j = 0; j <  _clusterIndices.size(); j++){
		geometry_msgs::Point centroid = _clusterDetections.cone_detections[j].position; 
        Point centroid3d(centroid.x, centroid.y, centroid.z);

		for(int k = 0; k < _clusterIndices[j].indices.size(); k++){
			PclPoint &point = pcl_cloud->points[_clusterIndices[j].indices[k]];
			Point p(point.x,point.y,point.z);

            Point point_var = (p - centroid3d).array().square();
			
			var += point_var;
		}


        varArray.push_back(var);
		
	}

}

std::vector<pcl::PointIndices> const & getClusters( PclPointCloud::Ptr pointCloud ){
    
    _clusterIndices.clear();

    int C = 0;

    // Create KD tree
    pcl::search::KdTree<PclPoint>::Ptr tree(new pcl::search::KdTree<PclPoint>);
	tree->setInputCloud(pointCloud);

    // Create labels for each case: Undefined point(-1), Noise (-2)
    std::vector<int> label(pointCloud->points.size(), -1);

    //Loop through all the points of the pointcloud
    for (int i = 0; i < pointCloud->points.size() ; i++) {
        
        //Check if the point has been defined
        if (label[i] != -1) continue;

        std::vector<int> point_indices;
        std::vector<float> squared_distances;

        // Search for the neighbors
        tree->radiusSearch ( pointCloud->points[i] , 0.1, point_indices, squared_distances);

        int neighbors = point_indices.size();

        //Check if the point is NOISE (if it has a number of neighbors < number minimum of points to be considered a core point)
        if (neighbors < 1) {
            label[i] = -2; 
            continue;   
        }

        label[i] = ++C;
        
        // for( const auto Q: point_indices){
        for( size_t j = 0; j < point_indices.size(); j++){

            int Q = point_indices[j];
 
            //Check if the point was noise, now it is a border point (number of neighbors < minPoints)
            if ( label[Q] == -2 ) label[Q] = C;

            //Check if defined
            if ( label[Q] != -1 ) continue;

            label[Q] = C;
                        
            std::vector<int> neighbor_indices;
            std::vector<float> neighbor_distances;
            tree->radiusSearch ( pointCloud->points[Q] , 0.1, neighbor_indices, neighbor_distances);
            
            //Add the neighbors of the neighbors to the cluster depending on the minimum number of points
            if (neighbor_indices.size() >= 1){
                for(auto &neighbor_index: neighbor_indices ){ point_indices.push_back(neighbor_index);}
            }
        }

        pcl::PointIndices S;
        S.indices = point_indices;
        _clusterIndices.push_back(S);

    }

    return _clusterIndices;
}

visualization_msgs::Marker mark_def(visualization_msgs::Marker marker, int i){
    
        marker.id = i;

        marker.header.frame_id = "/base_laser";
        marker.ns = "my_namespace";
        marker.action = visualization_msgs::Marker::ADD;
        marker.header.stamp = ros::Time::now();
        marker.type = visualization_msgs::Marker::SPHERE;
        
        marker.scale.x = 0.13;
        marker.scale.y = 0.13;
        marker.scale.z = 0.13;
        
        marker.color.r=1;
        marker.color.g=0.5;
        marker.color.b=0.5;
        marker.color.a=1;

        marker.pose.position.z = 0;
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;

        return marker;
}

std::vector<visualization_msgs::Marker> pubPointCloud(){
    
    std::vector<visualization_msgs::Marker> markerArrayPointCloud;
    markerArrayPointCloud.resize(pcl_cloud->points.size());
    
    for(int i=0; i<pcl_cloud->points.size(); i++){

        markerArrayPointCloud[i].pose.position.x = pcl_cloud->points[i].x;
        markerArrayPointCloud[i].pose.position.y = pcl_cloud->points[i].y;
        
        markerArrayPointCloud[i].id = i;

        markerArrayPointCloud[i].header.frame_id = "/base_laser";
        markerArrayPointCloud[i].ns = "my_namespace1";
        markerArrayPointCloud[i].action = visualization_msgs::Marker::ADD;
        markerArrayPointCloud[i].header.stamp = ros::Time::now();
        markerArrayPointCloud[i].type = visualization_msgs::Marker::SPHERE;
        
        markerArrayPointCloud[i].scale.x = 0.07;
        markerArrayPointCloud[i].scale.y = 0.07;
        markerArrayPointCloud[i].scale.z = 0.07;
        
        markerArrayPointCloud[i].color.r=1;
        markerArrayPointCloud[i].color.g=0;
        markerArrayPointCloud[i].color.b=0;
        markerArrayPointCloud[i].color.a=1;

        markerArrayPointCloud[i].pose.position.z = 0;
        markerArrayPointCloud[i].pose.orientation.x = 0.0;
        markerArrayPointCloud[i].pose.orientation.y = 0.0;
        markerArrayPointCloud[i].pose.orientation.z = 0.0;
        markerArrayPointCloud[i].pose.orientation.w = 1.0;     
    }
    return markerArrayPointCloud;
}

void Perception::runAlgorithm() {
	// run processing

	laser_geometry::LaserProjection projector;
    
	//transform laser to pointcloud2
	projector.projectLaser(_input, cloud);
	//ROS_WARN("%d\n", _input.ranges.size());
    //ROS_WARN("%f\n", cloud.height*cloud.width);
	//converts pointcloud2 to pcl pointcloud
	pcl::fromROSMsg(cloud, *pcl_cloud);

	//calculate indices that are in clusters
	std::vector<pcl::PointIndices> idx;

	idx = getClusters(pcl_cloud);

	computeCentroids(idx,pcl_cloud);

    centroid2msg(idx);
    
    _output = _clusterDetections;

    std::vector<visualization_msgs::Marker> markerArray(_clusterDetections.cone_detections.size());
    ROS_WARN("Centroids: %d", _clusterDetections.cone_detections.size());
    for(int i = 0; i<_clusterDetections.cone_detections.size(); i++){
        
        markerArray[i].pose.position.x = _clusterDetections.cone_detections[i].position.x;
        markerArray[i].pose.position.y = _clusterDetections.cone_detections[i].position.y;

        markerArray[i]=mark_def(markerArray[i], i);
        
    }

    _markerArray.markers=markerArray;
    _markerArrayPointCloud.markers=pubPointCloud();

    int tamanho=allCentroids.size();
    for(int i = 0; i<tamanho; i++)
        allCentroids.pop_back();



}

