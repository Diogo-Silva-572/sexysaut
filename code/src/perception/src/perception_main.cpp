#include "ros/ros.h"
#include "perception/perception_handle.hpp"


int main(int argc, char** argv) {

	ros::init(argc, argv, "perception");

	ros::NodeHandle nh;
	PerceptionHandle perceptionHandle(nh);

	ros::Rate loop_rate(50);

	while (ros::ok()) {

		perceptionHandle.run();	// run node

			// Keeps node alive basically
		ros::spinOnce();
	}

	return 0;
}
