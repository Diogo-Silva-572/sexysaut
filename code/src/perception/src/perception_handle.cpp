#include "perception/perception_handle.hpp"		// include header file


PerceptionHandle::PerceptionHandle(ros::NodeHandle &nodeHandle): _nodeHandle(nodeHandle) {

	_pubConesViz=_nodeHandle.advertise<visualization_msgs::MarkerArray>("cones_rviz", 1);
    _subInput=_nodeHandle.subscribe("/scan", 1, &PerceptionHandle::inputCallback, this);
    _pubOutput=_nodeHandle.advertise<perception::ConeDetections>("cone_coords", 1);
	_pubPointCloudViz=_nodeHandle.advertise<visualization_msgs::MarkerArray>("cloud_rviz",1);
}

void PerceptionHandle::run() {
	_subpub.runAlgorithm();
	_pubOutput.publish(_subpub.getPointCoords());// get info from algorithm and publish it
	_pubConesViz.publish(_subpub.getConesRviz());
	_pubPointCloudViz.publish(_subpub.getPointCloudRviz());
}

void PerceptionHandle::inputCallback(const sensor_msgs::LaserScan& input) {
	_subpub.setLaser(input);
}
