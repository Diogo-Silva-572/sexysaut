#ifndef PERCEPTION_HPP
#define PERCEPTION_HPP

#include "ros/ros.h"
#include <stdio.h>
#include <sensor_msgs/LaserScan.h>
#include "geometry_msgs/Point.h"
#include <tf/transform_listener.h>
#include <laser_geometry/laser_geometry.h>
#include "tf/message_filter.h"
#include "tf/exceptions.h"
#include "message_filters/subscriber.h"
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include "perception/cone.h"
#include "perception/ConeDetections.h"
#include <pcl/common/transforms.h>
#include <pcl/common/common.h>
#include <visualization_msgs/MarkerArray.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <pcl/sample_consensus/sac_model_circle.h>


typedef pcl::PointXYZI PclPoint;
typedef pcl::PointCloud<PclPoint> PclPointCloud;
typedef Eigen::Vector3d Point;

class Perception {
	
	public:

		// constructor
		Perception();

		// getters
		perception::ConeDetections getPointCoords();
		visualization_msgs::MarkerArray getConesRviz();
		visualization_msgs::MarkerArray getPointCloudRviz();

		// setters
		void setLaser(const sensor_msgs::LaserScan&);
		
		// methods
		void runAlgorithm();

	private:

		// attributes
		sensor_msgs::LaserScan _input;
		perception::ConeDetections _output;
		visualization_msgs::MarkerArray _markerArray;
		visualization_msgs::MarkerArray _markerArrayPointCloud;
		
};

#endif
