#ifndef PERCEPTION_HANDLE_HPP
#define PERCEPTION_HANDLE_HPP

#include "ros/ros.h"
#include "perception/perception.hpp"	// include the Example class (header file)

class PerceptionHandle {

	public:

		// constructor
		PerceptionHandle(ros::NodeHandle &nodeHandle);

		// methods
		void run();

	private:

		// attributes
		ros::NodeHandle _nodeHandle;
		ros::Publisher _pubOutput;
		ros::Publisher _pubConesViz;
		ros::Publisher _pubPointCloudViz;
		ros::Subscriber _subInput;
		Perception _subpub;		// instance of the algorithm itself

		// callbacks
		void inputCallback(const sensor_msgs::LaserScan& input);

};

#endif
